## default.xml

Initialize an environment with:

- **poky**: reference distribution of the Yocto Project.
- **meta-openembedded**: collection of layers for the OE-core. universe

This manifest file is intended to be used by developpers who want to test the project on qemu or on an unsupported board.

## raspberrypi.xml

Initialize an environment with the same layers as in *default.xml*, and adds the following:

- **meta-raspberrypi**: Yocto BSP layer for raspberrypi.
